﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeTaskLogService
    {
        public TimesheetDb db { get; }
        public EmployeeTaskLogService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public List<WorkLogViewModel> GetEmployeeTimesheet(int employeeid, DateTime startdate, DateTime enddate)
        {
            var groupedtasks = db.EmployeeTaskLogs
                          .Where(emp => emp.EmployeeId == employeeid
                          && emp.TaskDate >= startdate
                          && emp.TaskDate <= enddate).GroupBy(emp => emp.TaskId);

            List<WorkLogViewModel> workitems = new List<WorkLogViewModel>();

            foreach (var group in groupedtasks) 
            {
                var worklog = new WorkLogViewModel();
                worklog.taskid = group.Key;
                worklog.days = new WorkDay[7];
                foreach (var groupedItem in group)
                {
                    worklog.days[(int)groupedItem.TaskDate.DayOfWeek] = new WorkDay();
                    worklog.days[(int)groupedItem.TaskDate.DayOfWeek].date = groupedItem.TaskDate;
                    worklog.days[(int)groupedItem.TaskDate.DayOfWeek].hoursWorked = groupedItem.TaskHours;
                }
                workitems.Add(worklog);
            }

            return workitems;
        }

        public bool SaveEmployeeTimesheet(TimesheetViewModel timesheet)
        {
            bool result = false;

            try
            {
                db.RemoveRange(db.EmployeeTaskLogs.Where(emp => emp.EmployeeId == timesheet.EmployeeId
                          && emp.TaskDate >= timesheet.WeekStartDate
                          && emp.TaskDate <= timesheet.WeekEndDate));
                db.SaveChanges();


                db.AddRange(timesheet.WorkLogs);
                db.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                //log the exception
            }
            return result;
        }

    }
}
