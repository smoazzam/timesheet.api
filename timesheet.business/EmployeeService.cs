﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public IQueryable GetEmployeesEffort(DateTime startdate, DateTime enddate)
        {
            var result = from employee in this.db.Employees
                         select new
                         {
                             id = employee.Id,
                             code = employee.Code,
                             name = employee.Name,
                             total = this.db.EmployeeTaskLogs.Where(log => log.EmployeeId == employee.Id
                             && log.TaskDate >= startdate && log.TaskDate <= enddate).Sum(hr => hr.TaskHours)
                         };
 
            return result;
        }
    }
}
