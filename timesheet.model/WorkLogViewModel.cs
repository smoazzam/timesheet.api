﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class WorkLogViewModel
    {
        public int taskid { get; set; }
        public WorkDay[] days { get; set; }

    }

    public class WorkDay
    {
        public DateTime date { get; set; }
        public int hoursWorked { get; set; }
    }
}
