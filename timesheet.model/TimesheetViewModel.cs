﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class TimesheetViewModel
    {
        public int EmployeeId { get; set; }
        public DateTime WeekStartDate { get; set; }
        public DateTime WeekEndDate { get; set; }
        public List<EmployeeTaskLog> WorkLogs { get; set; }
    }
}
