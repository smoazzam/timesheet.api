﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly EmployeeTaskLogService employeeTaskLogService;
        public TimesheetController(EmployeeTaskLogService employeeTaskLogService)
        {
            this.employeeTaskLogService = employeeTaskLogService;
        }

        [HttpGet("get")]
        public IActionResult GetEmployeeTimesheet(int employeeid, DateTime startdate, DateTime enddate)
        {
            var items = this.employeeTaskLogService.GetEmployeeTimesheet(employeeid, startdate, enddate);
            return new ObjectResult(items);
        }

        [HttpPost("save")]
        public IActionResult SaveEmployeeTimesheet([FromBody] JObject objData)
        { 
            dynamic jsonData = objData;
            JArray worklogsJson = jsonData.worklogs;

            TimesheetViewModel timesheet = new TimesheetViewModel();
            timesheet.EmployeeId = jsonData.employeeid;
            timesheet.WeekStartDate = jsonData.startdate;
            timesheet.WeekEndDate = jsonData.enddate;
            timesheet.WorkLogs = new List<EmployeeTaskLog>();

            foreach (var item in worklogsJson)
            {
                timesheet.WorkLogs.Add(item.ToObject<EmployeeTaskLog>());
            }

            bool result = this.employeeTaskLogService.SaveEmployeeTimesheet(timesheet);
            return result ? StatusCode(StatusCodes.Status200OK) : StatusCode(StatusCodes.Status400BadRequest);
        }
    }
}